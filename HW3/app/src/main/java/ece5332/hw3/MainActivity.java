package ece5332.hw3;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends Activity {

    final int MAX_KEYS = 63;
    final int WIN_CONDITION = 50;
    final int MAX_PROGRESS = 90;

    ProgressBar myBarHorizontal;
    ProgressBar p1Bar;
    ProgressBar p2Bar;
    ProgressBar totalBar;
    ProgressBar remainingBar;

    //ProgressBar myBarCircular;
    TextView lblTopCaption;
    TextView p1Text;
    TextView p2Text;
    TextView totalText;
    TextView remainingText;

    EditText txtDataBox;
    Button btnDoSomething;
    Button btnDoItAgain;

    int progressStepClock = 1;
    int progressStepTotal = 7;
    int p1_keys;
    int p2_keys;
    int p1_increment;
    int p2_increment;
    int remaining_keys ;
    int clockcount = 0;
    int totalKeys = 7;
    int p1_grabtime;
    int p2_grabtime;

    int p1_keytime;
    int p2_keytime;

    //int val = 0;

    long startingMills = System.currentTimeMillis();
    boolean isRunning = true;
    String CLOCK = "Countdown: ";
    String TOTAL = "Total Keys: ";
    String P1_TEXT = "Player 1 Keys: ";
    String P2_TEXT = "Player 2 Keys: ";
    String REMAINING_TEXT = "Remaining Keys: ";


    Handler myHandler = new Handler();


    // FOREGROUND
    // this foreground Runnable works on behave of the background thread,
    // its mission is to update the main UI which is unreachable to back worker
    private Runnable foregroundRunnable_Clock = new Runnable() {
        @Override
        public void run() {
            try {
                // advance ProgressBar
                myBarHorizontal.incrementProgressBy(progressStepClock);

                if(clockcount < 90 ) {
                    clockcount += progressStepClock;
                }

                // are we done yet?
                else {
                    lblTopCaption.setText("Time is up!");
                    if (p1_keys > p2_keys){
                        p1Text.setText("WINNER!" + "(" + p1_keys + ")");
                    }
                    if (p2_keys > p1_keys){
                        p2Text.setText("WINNER!" + "(" + p2_keys + ")");
                    }
                    //myBarHorizontal.setVisibility(View.INVISIBLE);
                    //myBarCircular.setVisibility(View.INVISIBLE);
                    isRunning = false;
                    //btnDoItAgain.setEnabled(true);
                }

                lblTopCaption.setText(CLOCK + clockcount + "/90 seconds");
            } catch (Exception e) {
                Log.e("<<foregroundTask>>", e.getMessage());
            }
        }
    };


    // foregroundTask
    // ////////////////////////////////////////////////////////////////////
    // BACKGROUND
    // this is the back runnable that executes the slow work
    // ////////////////////////////////////////////////////////////////////
    private Runnable backgroundTask_Clock = new Runnable() {
        @Override
        public void run() {
            // busy work goes here...
            try {
                for (int n = 0; n <= MAX_PROGRESS; n++) {
                    // this simulates 1 sec. of busy activity
                    Thread.sleep(1000);
                    myHandler.post(foregroundRunnable_Clock);
                }
            } catch (InterruptedException e) {
                Log.e("<<foregroundTask>>", e.getMessage());
            }

        }// run
    };// backgroundTask

    //Handler myHandler_Total = new Handler();

    private Runnable foregroundRunnable_Total = new Runnable() {
        @Override

        public void run() {
            try {
                // update UI, observe globalVar is changed in back thread
                // advance ProgressBar
                totalBar.incrementProgressBy(progressStepTotal);
                remainingBar.incrementProgressBy(progressStepTotal);

                totalKeys += progressStepTotal;
                remaining_keys += progressStepTotal;


                totalText.setText(TOTAL + totalBar.getProgress());
                remainingText.setText(REMAINING_TEXT + remainingBar.getProgress());

                // are we done yet?
                if (totalKeys > totalBar.getMax()) {
                    //     totalBar.setVisibility(View.INVISIBLE);
                    totalText.setText("No more keys!");

                    isRunning = false;
                }
            } catch (Exception e) {
                Log.e("<<foregroundTask1>>", e.getMessage());
            }
        }
    };
    // ///////////////////////////////////////////////////////////////////
    private Runnable backgroundTask_Total = new Runnable() {
        @Override
        public void run() {
            // busy work goes here...

            try {
                for (int i = 0; i < 1; i++) {
                    // this simulates 1 sec. of busy activity
                    Thread.sleep(0);
                    myHandler.post(foregroundRunnable_Total);
                    for (int j = 0; j <=7; j++){
                        Thread.sleep(10000);
                        myHandler.post(foregroundRunnable_Total);
                    }
                }
            } catch (InterruptedException e) {
                Log.e("<<foregroundTask1>>", e.getMessage());
            }

        }// run
    };

    private Runnable foregroundRunnable_P1 = new Runnable() {
        @Override
        public void run() {
            try {
                //Random
                Random rnd_p1 = new Random();
                p1_increment = rnd_p1.nextInt(remaining_keys) + 1;

                // advance ProgressBar
                p1Bar.incrementProgressBy(p1_increment);
                remainingBar.incrementProgressBy(-p1_increment);

                remaining_keys -= p1_increment;
                p1_keys += p1_increment;

                p1Text.setText(P1_TEXT + p1Bar.getProgress() + "/50");
                remainingText.setText(REMAINING_TEXT + remainingBar.getProgress());

                // are we done yet?
                if (p1_keys >= p1Bar.getMax()) {
                    p1Text.setText("Winner!" + "(" + p1_keys + ")");
                    //myBarHorizontal.setVisibility(View.INVISIBLE);
                    //myBarCircular.setVisibility(View.INVISIBLE);
                    isRunning = false;
                }

            } catch (Exception e) {
                Log.e("<<foregroundTask2>>", e.getMessage());
            }
        }
    };

    private Runnable backgroundTask_P1 = new Runnable() {
        @Override
        public void run() {
            // busy work goes here...
            while(isRunning){
                try {
                    // this simulates 1 sec. of busy activity
                    Random rnd_1 = new Random();
                    p1_grabtime = rnd_1.nextInt(14) + 1;

                    Thread.sleep(p1_grabtime * 500);
                    // wake up foregroundRunnable delegate to speak for you
                    myHandler.post(foregroundRunnable_P1);

                } catch (InterruptedException e) {
                    Log.e("<<foregroundTask2>>", e.getMessage());
                }
            }

        }// run
    };


    private Runnable foregroundRunnable_P2 = new Runnable() {
        @Override
        public void run() {
            try {


                //Random
                Random rnd_p2 = new Random();
                p2_increment = rnd_p2.nextInt(remaining_keys) + 1;


                // advance ProgressBar
                p2Bar.incrementProgressBy(p2_increment);
                remainingBar.incrementProgressBy(-p2_increment);

                p2_keys += p2_increment;
                remaining_keys-=p2_increment;

                p2Text.setText(P2_TEXT + p2_keys + "/50");
                remainingText.setText(REMAINING_TEXT + remainingBar.getProgress());

                //remainingBar.setProgress(32);

                // are we done yet?
                if (p2_keys >= p2Bar.getMax()) {
                    p2Text.setText("Winner" + "(" + p2_keys + ")");
                    //myBarHorizontal.setVisibility(View.INVISIBLE);
                    //myBarCircular.setVisibility(View.INVISIBLE);
                    isRunning = false;
                }

            } catch (Exception e) {
                Log.e("<<foregroundTask3>>", e.getMessage());
            }
        }
    };

    private Runnable backgroundTask_P2 = new Runnable() {
        @Override
        public void run() {
            // busy work goes here...
            while(isRunning){
                try {
                    // this simulates 1 sec. of busy activity
                    Random rnd_2 = new Random();
                    p2_grabtime = rnd_2.nextInt(14) + 1;

                    Thread.sleep(p2_grabtime * 500);
                    // wake up foregroundRunnable delegate to speak for you
                    myHandler.post(foregroundRunnable_P2);

                } catch (InterruptedException e) {
                    Log.e("<<foregroundTask3>>", e.getMessage());
                }
            }

        }// run
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // Java-UI wiring
        //txtDataBox = (EditText) findViewById(R.id.txtBox1);
        lblTopCaption = (TextView) findViewById(R.id.lblTopCaption);
        totalText = (TextView) findViewById(R.id.totalText);
        p1Text = (TextView) findViewById(R.id.p1Text);
        p2Text = (TextView) findViewById(R.id.p2Text);
        remainingText = (TextView) findViewById(R.id.remainingText);

        myBarHorizontal = (ProgressBar) findViewById(R.id.myBarHor);
        totalBar = (ProgressBar) findViewById(R.id.totalBar);
        p1Bar = (ProgressBar) findViewById(R.id.p1Bar);
        p2Bar = (ProgressBar) findViewById(R.id.p2Bar);
        remainingBar = (ProgressBar) findViewById(R.id.remainingBar);

        btnDoItAgain = (Button) findViewById(R.id.btnDoItAgain);
        btnDoItAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onStart();
            }// onClick
        });// setOnClickListener

        btnDoSomething = (Button) findViewById(R.id.btnDoSomething);
        btnDoSomething.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(MainActivity.this,
                        "Canceled >> \n", Toast.LENGTH_SHORT).show();
            }// onClick
        });
    }// onCreate

    // ///////////////////////////////////////////////////////////////////
    @Override
    protected void onStart() {
        super.onStart();
        isRunning = true;
        // prepare UI components
        //txtDataBox.setText("");
        //btnDoItAgain.setEnabled(true);

        lblTopCaption.setText("Countdown: ");
        totalText.setText("Total Keys: ");
        p1Text.setText("Player 1 Keys: ");
        p2Text.setText("Player 2 Keys: ");
        remainingText.setText("Remaining Keys: ");

        // reset and show progress bars
        myBarHorizontal.setMax(MAX_PROGRESS);
        myBarHorizontal.setProgress(0);
        myBarHorizontal.setVisibility(View.VISIBLE);

        totalBar.setMax(MAX_KEYS);
        totalBar.setProgress(0);
        totalBar.setVisibility(View.VISIBLE);

        p1Bar.setMax(WIN_CONDITION);
        p1Bar.setProgress(0);
        p1Bar.setVisibility(View.VISIBLE);

        p2Bar.setMax(WIN_CONDITION);
        p2Bar.setProgress(0);
        p2Bar.setVisibility(View.VISIBLE);

        remainingBar.setMax(MAX_KEYS);
        remainingBar.setProgress(0);
        remainingBar.setVisibility(View.VISIBLE);

        int p1_keys = 0;
        int p2_keys = 0;
        int p1_increment = 0;
        int p2_increment = 0;
        int remaining_keys = 0;



        // create background thread were the busy work will be done
        Thread ClockBackgroundThread = new Thread(backgroundTask_Clock);
        ClockBackgroundThread.start();

        Thread TotalBackGroundThread = new Thread(backgroundTask_Total);
        TotalBackGroundThread.start();

        Thread P1_BackGroundThread = new Thread(backgroundTask_P1);
        P1_BackGroundThread.start();


        Thread P2_BackGroundThread = new Thread(backgroundTask_P2);
        P2_BackGroundThread.start();

    }

}// ThreadsPosting

